This program is intended to be used with the 'screen' terminal program in OS X
or Linux/Unix. It should also be possible to run it using Putty or a similar 
terminal emulator running on Windows.

To communicate with the Arduino using 'screen':

In a terminal window, type:  ls /dev/tty.*  to get the ID of the serial port.

Then enter:  screen -L /dev/tty.usbXXXXX 9600

To exit screen, type ctrl/a and then d for "Detach." 

Description of the main menu options:

p (Position eyes):
Manually position the eyes using the 1, 2, 3 and 4 keys to step the LX, LY, RX and RY motors respectively. Holding the shift key reverses direction. The motors move one step per keypress, though, depending on the host OS, keys may auto-repeat if held down. For rapid, coarse positioning, the a, s, d and f keys work as above, but in 10 step increments.

s (Set target points):
Create a sequence of movements by manually positioning the eyes (via the keyboard) to desired target positions, and loading the points into an array to be "played back" later using the 'm' (Move through points) option. Upon conclusion, target point assignment statements are saved to the screen logfile, so they may be reused after the Arduino is rebooted/powered down. These assignment statements may be pasted into the LoadSavedTarget function in the source code.

a (Adjust target points):
Move through a set of points defined with the 's' option at full speed, allowing each point to be adjusted to compensate for the effect of inertia not present when slowly defining points one step at a time. Accuracy may be improved by choosing this option multiple times. As with the above option, the point assignment statements are saved to the screen logfile upon exit.

m (Move through target points):
Move through the sequence of points defined with options 's' and 'a.' After each cycle, the location of the origin may be adjusted if drift has occurred.

t (Start movement task)
Invokes movement commands hard coded into the MovementTask function.

z (Initialize present position to 0,0,0,0)
Makes the present position the origin point for further movements.

l (Load saved targets)
Loads a movement sequence previously retrieved from the screen logfile and pasted into the LoadSavedTargets function, so that the sequence may be "replayed" with the 'm' or 'a' options.